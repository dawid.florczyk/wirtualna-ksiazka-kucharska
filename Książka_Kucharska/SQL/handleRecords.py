from SQL.Database import *


class handleRecords(SQL):
    def __init__(self):
        SQL.__init__(self)

    def addRecord(self, category, subcategory, name, ingredients, time, difficulty, description):
         try:
            self.cursor.execute("""INSERT INTO Recipes 
            (Category, SubCategory, Name, Ingredients, Time, Difficulty, Description) VALUES (?, ?, ?, ?, ?, ?, ?)""",
                                     (category, subcategory, name, ingredients, time, difficulty, description))
            self.connection.commit()
            del self
         except:
             print('Error')

    def deleteRecord(self, ID):
        self.cursor.execute("DELETE FROM Recipes WHERE ID = (?)", (ID,))
        self.connection.commit()
        del self
