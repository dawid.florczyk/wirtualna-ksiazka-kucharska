import sqlite3

class SQL:
    def __init__(self):
        self.connection = sqlite3.connect("SQL\\Recipes.db")
        self.cursor = self.connection.cursor()

    def createTables(self):
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS Recipes
            (ID INTEGER PRIMARY KEY,
            Category TEXT,
            SubCategory TEXT,
            Name TEXT,
            Ingredients TEXT,
            Time INTEGER,
            Difficulty TEXT,
            Description TEXT);""")
        self.connection.commit()

SQLObject = SQL()
SQLObject.createTables()





