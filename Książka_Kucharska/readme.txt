Projektem jest wirtualna książka kucharska napisana w języku Python 3.8 i w bibliotece PyQt5. 
Program obsługuje dodawanie i usuwanie przepisów za pomocą użycia bazy danych SQL.
Biblioteki i ich odpowiednie wersje znajdują się w pliku requirements.txt a sam program uruchamia się poprzez plik Main.py