from PyQt5 import QtWidgets
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from SQL.Database import *
from SQL.handleRecords import *
from PyQt5.QtCore import *
import sys


class addingException(Exception):
    def __init__(self, labelText, xpos, ypos):
        Exception.__init__(self)
        self.labelText = labelText
        self.xpos = xpos
        self.ypos = ypos


class Window:
    def __init__(self):
        self.app = QApplication(sys.argv)
        self.win = QStackedWidget()
        self.recordsList = ["Category", "SubCategory", "Name", "Ingredients", "Time", "Difficulty", "Description"]
        self.Categories = ['Mięsne', 'Wegańskie', 'Wegetarianskie', 'Rybne']
        self.CategoryCheckboxDict = {}
        self.buttonDict = {}
        self.viewDict = {}
        self.recipeDict = {}

        self.win.setGeometry(200, 200, 400, 400)
        self.win.setWindowTitle('Cook Book')
        self.win.setWindowIcon(QtGui.QIcon('GUI/OIP.png'))

        self.label = QtWidgets.QLabel(self.win)
        self.label.setText('Cook book')
        self.label.move(160, 20)
        self.label.setFont(QFont('Calibri', 16))
        self.label.adjustSize()

        self.b1 = QtWidgets.QPushButton(self.win)
        self.b1.setText('Browse recipes')
        self.b1.move(160, 120)
        self.b1.clicked.connect(self.showViewWindow)

        self.b2 = QtWidgets.QPushButton(self.win)
        self.b2.setText('Add recipe')
        self.b2.move(160, 200)
        self.b2.clicked.connect(self.showAddWindow)

        self.b3 = QtWidgets.QPushButton(self.win)
        self.b3.setText('Quit')
        self.b3.move(160, 280)
        self.b3.clicked.connect(self.exit)

    def showWindow(self):
        self.win.show()
        self.app.exec_()

    def showMainWindow(self):
        self.win.show()

    def showViewWindow(self):
        self.viewWindow()
        self.viewWid.show()
        self.win.close()

    def viewWindow(self):
        self.viewWid = QStackedWidget()
        self.viewWid.setGeometry(250, 300, 450, 450)
        self.viewWid.setWindowTitle('View recipe')
        self.viewWid.setWindowIcon(QtGui.QIcon('GUI/OIP.png'))
        self.ID = None

        self.setUpViewingList()
        self.prepareRecipes()
        self.createCategoryCheckboxes()

        applyButton = QPushButton('Apply', self.viewWid)
        applyButton.adjustSize()
        applyButton.move(173, 120)
        applyButton.clicked.connect(self.applyCheckboxes)
        doneButton = QtWidgets.QPushButton(self.viewWid)
        doneButton.setText('Done')
        doneButton.setGeometry(400, 400, 50, 50)
        doneButton.clicked.connect(self.closeViewWindow)
        doneButton.clicked.connect(self.showMainWindow)

    def prepareRecipes(self):
        self.nameList = []
        SQLObject.cursor.execute("SELECT Name FROM Recipes")
        for i in SQLObject.cursor.fetchall():
            self.nameList.append(str(*i))
        for i in range(len(self.nameList)):
            self.recipeButton = QPushButton(self.nameList[i], self.viewWid)
            self.recipeButton.adjustSize()
            self.recipeButton.setFixedWidth(270)
            self.recipeButton.hide()
            self.recipeDict.update({self.nameList[i]:self.recipeButton})

        for key, value in self.recipeDict.items():
            self.connectButtons(key, value)

    def connectButtons(self, key, value):
        value.clicked.connect(lambda: self.showInfoWindow(key))

    def applyCheckboxes(self):
        activeCheckboxes = []
        activeRecipes = []
        for value in self.recipeDict.values():
            value.hide()
        for key, value in self.CategoryCheckboxDict.items():
            if value.isChecked():
                activeCheckboxes.append(key)
        for i in activeCheckboxes:
            if 'Wszystkie' in activeCheckboxes:
                SQLObject.cursor.execute("SELECT Name FROM Recipes")
                for i in SQLObject.cursor.fetchall():
                    activeRecipes.append(str(*i))
                break
            else:
                SQLObject.cursor.execute("SELECT Name FROM Recipes WHERE Category = (?)", (i,))
                for i in SQLObject.cursor.fetchall():
                    activeRecipes.append(str(*i))

        ypos = 150
        for key, value in self.recipeDict.items():
            for i in range(len(activeRecipes)):
                if activeRecipes[i] == key:
                        value.move(96, ypos)
                        ypos += 25
                        value.show()


    def setUpViewingList(self):
        titleLabel = QLabel('Recipe viewing', self.viewWid)
        titleLabel.move(170, 0)
        titleLabel.setFont(QFont('Calibri', 12))

    def createCategoryCheckboxes(self):
        for i in range(len(self.Categories)):
            self.categoryCheckbox = QCheckBox(self.viewWid)
            self.CategoryCheckboxDict.update({self.Categories[i] : self.categoryCheckbox})
        self.checkBoxAll = QCheckBox('Wszystkie', self.viewWid)
        self.checkBoxAll.move(205, 40)
        self.checkBoxAll.adjustSize()
        xpos = 160
        for key, value in self.CategoryCheckboxDict.items():
            if key == 'Mięsne' or key == 'Wegańskie':
                value.setText(key)
                value.move(xpos, 70)
                xpos += 80
                value.adjustSize()
            else:
                xpos -= 80
                value.move(xpos, 90)
                value.setText(key)
                value.adjustSize()
        self.CategoryCheckboxDict.update({'Wszystkie': self.checkBoxAll})

    def closeViewWindow(self):
        self.recipeDict.clear()
        self.CategoryCheckboxDict.clear()
        self.viewWid.close()

    def showInfoWindow(self, key):
        self.infoWindow(key)
        self.infoWid.show()

    def infoWindow(self, key):
        self.infoWid = QStackedWidget()
        self.infoWid.setGeometry(250, 300, 450, 450)
        self.infoWid.setWindowTitle(key + ' info')
        self.infoWid.setWindowIcon(QtGui.QIcon('GUI/OIP.png'))
        self.setInfoWindow(key)

    def setInfoWindow(self, key):
        titleLabel = QLabel(key + ' viewing', self.infoWid)
        titleLabel.move(20, 0)
        titleLabel.setFont(QFont('Calibri', 12))
        titleLabel.adjustSize()
        dictInfo = {}
        dictInfo2 = {}
        recipeInfo = []
        for i in self.recordsList:
            label = QLabel(i + ':'  ,self.infoWid)
            dictInfo.update({i:label})
        ypos = 30
        for value in dictInfo.values():
            value.move(20, ypos)
            ypos += 50
        SQLObject.cursor.execute("SELECT * FROM Recipes WHERE Name = (?)", (key,))
        for i in SQLObject.cursor.fetchall():
            for j in i:
                recipeInfo.append(j)
        ID = recipeInfo[0]
        recipeInfo.pop(0)
        for i in range(len(recipeInfo)):
            textEdit = QTextEdit(self.infoWid)
            textEdit.setReadOnly(True)
            dictInfo2.update({recipeInfo[i]:textEdit})
        ypos = 30
        for key, value in dictInfo2.items():
            value.setText(str(key))
            value.setFixedHeight(45)
            value.setFixedWidth(350)
            value.move(90, ypos)
            value.adjustSize()
            ypos += 50

        doneButton = QtWidgets.QPushButton(self.infoWid)
        doneButton.setText('Done')
        doneButton.setGeometry(400, 400, 50, 50)
        doneButton.clicked.connect(self.closeInfoWindow)
        deleteButton = QtWidgets.QPushButton(self.infoWid)
        deleteButton.setText('Delete Recipe')
        deleteButton.setGeometry(0, 400, 80, 50)
        deleteButton.clicked.connect(lambda: self.removeRecipe(ID))

    def closeInfoWindow(self):
        self.applyCheckboxes()
        self.infoWid.close()

    def removeRecipe(self, ID):
        removalObjects = handleRecords()
        removalObjects.deleteRecord(ID)
        self.closeInfoWindow()


    def showAddWindow(self):
        self.addWindow()
        self.addWid.setWindowIcon(QtGui.QIcon('GUI/OIP.png'))
        self.addWid.show()
        self.win.close()

    def addWindow(self):
        self.addWid = QStackedWidget()
        self.addWid.setGeometry(250, 300, 450, 450)
        self.addWid.setWindowTitle('Add Recipe')
        titleLabel = QLabel('Recipe Adding', self.addWid)
        titleLabel.setFont(QFont('Calibri', 13))
        titleLabel.move(170, 0)

        self.list = []
        for i in range(len(self.recordsList)):
            self.addButton = QtWidgets.QTextEdit(self.addWid)
            self.addButton.setFixedHeight(40)
            self.addButton.setFixedWidth(280)
            self.addButton.adjustSize()
            self.buttonDict.update({self.recordsList[i]:self.addButton})
        xPos = 40
        for i in range(len(self.recordsList)):
            xPos += 45
            self.createAddWindowLabels(self.recordsList[i], xPos)
        xPos = 30
        for key, value in self.buttonDict.items():
            xPos += 45
            self.createAddWindowButtons(key, value, xPos)

        self.confirmationLabel = QtWidgets.QLabel(self.addWid)
        self.confirmationLabel.move(180, 400)
        self.confirmationLabel.setFont(QFont('Calibri', 12))
        self.confirmationLabel.setText('In-progress')

        showCategories = QLabel(self.addWid)
        showCategories.move(60, 40)
        string = 'Categories: '
        for i in range(len(self.Categories)):
            if i == 3:
                string += str(self.Categories[i]) + '. '
            else:
                string += str(self.Categories[i]) + ', '

            showCategories.setText(string)
            showCategories.setFont(QFont('Calibri', 12))
            showCategories.adjustSize()

        returnButton = QtWidgets.QPushButton(self.addWid)
        returnButton.setText('Return')
        returnButton.setGeometry(0, 400, 50, 50)
        returnButton.clicked.connect(self.closeAddWindow)
        returnButton.clicked.connect(self.showMainWindow)
        doneButton = QtWidgets.QPushButton(self.addWid)
        doneButton.setText('Done')
        doneButton.setGeometry(400, 400, 50, 50)
        doneButton.clicked.connect(self.tryAdding)

    def createAddWindowLabels(self, record, xPos):
        self.record = QtWidgets.QLabel(self.addWid)
        self.record.setText("Insert recipe's " + record + ": ")
        self.record.adjustSize()
        self.record.move(20, xPos)

    def createAddWindowButtons(self, key, value, xpos):
        value.move(165, xpos - 5)

    def tryAdding(self):
        for key, value in self.buttonDict.items():
            self.recipeDict.update({key:value.toPlainText()})
        try:
            self.executeAdding()
        except addingException as exec:
            self.confirmationLabel.setText(exec.labelText)
            self.confirmationLabel.move(exec.xpos, exec.ypos)
            self.confirmationLabel.adjustSize()


    def executeAdding(self):
        self.list.clear()

        for key, value in self.recipeDict.items():
            if value == '':
                raise addingException(key + " is currently empty", 130, 400)

        for i in range(len(self.recordsList)):
             for key, value in self.recipeDict.items():
                if self.recordsList[i] == key:
                    self.list.append(value)
        if len(self.list) == 7:
            SQLObject.cursor.execute("SELECT Name FROM Recipes")
            for i in SQLObject.cursor.fetchall():
                if self.list[2] == str(*i):
                    raise addingException('Name taken', 150, 400)
            if self.list[0] in self.Categories:
                addingObject = handleRecords()
                addingObject.addRecord(self.list[0], self.list[1], self.list[2], self.list[3], self.list[4], self.list[5], self.list[6])
                self.list.clear()
                self.recipeDict.clear()
                self.closeAddWindow()
                self.showMainWindow()
            else:
                raise addingException('Incorrect Category', 150, 400)
            
    def closeAddWindow(self):
        self.recipeDict.clear()
        self.addWid.close()

    def exit(self):
        SQLObject.connection.close()
        quit()
        sys.exit(self.app.exec_())








